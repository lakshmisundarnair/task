FROM --platform=linux/amd64 tiangolo/uvicorn-gunicorn-fastapi:python3.7

RUN apt-get update && apt-get install -y postgresql-client \
&& pip install flask gunicorn fastapi uvicorn pydantic SQLAlchemy psycopg2-binary tenacity==7.0.0 alembic

#RUN pip install fastapi uvicorn
COPY ./app /app
ARG SEED_DATA=no
ENV SEED_DATA=${SEED_DATA}
WORKDIR /app/
RUN chmod +x start-load.sh
RUN chmod 755 /app

ENV PYTHONPATH=/app
#CMD ["uvicorn", "app.index:app", "--host", "0.0.0.0", "--port", "80"]
