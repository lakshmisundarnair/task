from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model2 as models 
from code.endpoints.crud import crud1 as crud
from code.endpoints.schemas import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

router=APIRouter()
# Dependency
from code.database import get_db
@router.post("/user13/", response_model=schemas.User1, tags=["user13"])
def create_user(user: schemas.User1Create, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)


@router.get("/user13/", response_model=List[schemas.User1], tags=["user13"])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = crud.get_users(db, skip=skip, limit=limit)
    return users


@router.get("/user13/{user_id}", response_model=schemas.User1, tags=["user13"])
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user
