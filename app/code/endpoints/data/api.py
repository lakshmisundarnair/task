from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import models as models 
from code.endpoints.crud import crud as crud
from code import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user2=APIRouter()
# Dependency
from code.database import get_db
@user2.get("/get/jobs/{user_email}")#jobcheckuser
async def get_jobs(user_email: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        arr=[]
        query=db.query(models.Job)
        subquery=db.query(models.Application.job_id)
        in_expression=models.Job.id.notin_(subquery.filter(models.Application.user_id==db_user.id))
        db_job=query.filter(in_expression).all()
        for r in db_job:
            arr.append(r) 
        if(arr==[]):
            raise HTTPException(status_code=404, detail="New Jobs not yet created")

    return arr

@user2.delete("/application2/delete/{delid}")#delete application
async def delete_application(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_app(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Application not found in that id")
    else:
        delete=db.query(models.Application).filter(models.Application.id==delid).delete()
        db.commit()
    return db.query(models.Application).all()

@user2.delete("/job2/delete/{delid}")#delete job
async def delete_Job(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_job(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        delete=db.query(models.Job).filter(models.Job.id==delid).delete()
        delete_app=db.query(models.Application).filter(models.Application.job_id==delid).delete()
        db.commit()
    return db.query(models.Job).all()

@user2.put("/job2/edit/{id}")#update job
async def update_Job(id:int,job:schemas.Jobresponse,db: Session = Depends(get_db)):
    db_job=crud.chk_job(db,id=id)
    if (db_job==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        db_edit=crud.update_job(db,job,id=id)
        
    return db_edit
@user2.post("/create2/job/{mail}")#cretae
async def create_job(mail: str,job:schemas.Jobresponse,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=mail)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_chk=db.query(models.Job).filter( models.Job.title==job.title,models.Job.author_id==db_user.id,models.Job.company_name==job.company_name).all()
        if(db_chk !=[]):
            raise HTTPException(status_code=404, detail="This Job Already Exist")
        else:
            db_create=crud.create_job(db,job,id=db_user.id)
        return db_create



@user2.post("/apply2/{email}")#apply job
async def create_application(email: str,app:schemas.Applicationres,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=email)
    if (db_user is None):
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.chk_job(db,id=app.job_id)
        if (db_job==[]):
              raise HTTPException(status_code=404, detail="Job not found in that id")
        db_chk=db.query(models.Application).filter( models.Application.job_id==app.job_id,models.Application.user_id==db_user.id).first()
        if(db_chk is None):
            db_app=crud.create_application(db,app,id=db_user.id) 
        else:
            raise HTTPException(status_code=404, detail="Already applied")
    return db_app

@user2.post("/create_user2")
def create_user(input:schemas.UserCreate,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=input.email)
    if (db_user is not None):
        raise HTTPException(status_code=404, detail="User already found")
    else:
        db_user=crud.create_user(db,input)
    return db_user

