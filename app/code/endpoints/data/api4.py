from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model3 as models
from code.endpoints.crud import crud2 as crud 
from code.endpoints.schemas import schema1 as schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

router=APIRouter()
# Dependency
from code.database import get_db
@router.post("/ceate/job14/{mail}")#cretae
async def create_job(mail: str,job:schemas.Jobresponse1,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=mail)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_chk=db.query(models.Job3).filter( models.Job3.title==job.title,models.Job3.author_id==db_user.id,models.Job3.company_name==job.company_name).all()
        if(db_chk !=[]):
            raise HTTPException(status_code=404, detail="This Job Already Exist")
        else:
            db_create=crud.create_job(db,job,id=db_user.id)
        return db_create



@router.post("/aply14/{email}")#apply job
async def create_application(email: str,app:schemas.Applicationres1,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=email)
    if (db_user is None):
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.chk_job(db,id=app.job_id)
        if (db_job==[]):
              raise HTTPException(status_code=404, detail="Job not found in that id")
        db_chk=db.query(models.Application3).filter( models.Application3.job_id==app.job_id,models.Application3.user_id==db_user.id).first()
        if(db_chk is None):
            db_app=crud.create_application(db,app,id=db_user.id) 
        else:
            raise HTTPException(status_code=404, detail="Already applied")
    return db_app

@router.post("/ceate_user14")
def create_user(input:schemas.UserCreate1,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=input.email)
    if (db_user is not None):
        raise HTTPException(status_code=404, detail="User already found")
    else:
        db_user=crud.create_user(db,input)
    return db_user

