from sqlalchemy import Boolean, Column, ForeignKey, Integer, String,DateTime
from sqlalchemy.orm import relationship
from code.database import Base


class User1(Base):

    __tablename__ = "users1"

    user_id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    candidate_name = Column(String)
    is_active = Column(Boolean, default=True)

    applied_jobs = relationship("Apply1")
   


class Job1(Base):

    __tablename__ = "jobs1"

    job_id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    skill = Column(String, index=True)
    description = Column(String, index=True)
   

class Apply1(Base):

    __tablename__ = "apply1"

    apply_job_id = Column(Integer, primary_key=True, index=True)
    email = Column(String, ForeignKey("users1.email"))
    job_id = Column(Integer, ForeignKey("jobs1.job_id"))

    user_de = relationship('User1', foreign_keys=[email])
    job_de = relationship("Job1")
