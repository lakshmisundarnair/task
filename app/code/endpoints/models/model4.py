from sqlalchemy import Boolean, Column, ForeignKey, Integer, String,DateTime
from sqlalchemy.orm import relationship
from code.database import Base


class User2(Base):

    __tablename__ = "users2"

    user_id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    candidate_name = Column(String)
    is_active = Column(Boolean, default=True)

    applied_jobs = relationship("Apply2")
   


class Job2(Base):

    __tablename__ = "jobs2"

    job_id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    skill = Column(String, index=True)
    description = Column(String, index=True)
   

class Apply2(Base):

    __tablename__ = "apply2"

    apply_job_id = Column(Integer, primary_key=True, index=True)
    email = Column(String, ForeignKey("users2.email"))
    job_id = Column(Integer, ForeignKey("jobs2.job_id"))

    user_de = relationship('User2', foreign_keys=[email])
    job_de = relationship("Job2")
