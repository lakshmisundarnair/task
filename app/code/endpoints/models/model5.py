from sqlalchemy import Boolean, Column, ForeignKey, Integer, String,DateTime
from sqlalchemy.orm import relationship
from code.database import Base


class User4(Base):

    __tablename__ = "users4"

    user_id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    candidate_name = Column(String)
    is_active = Column(Boolean, default=True)

    applied_jobs = relationship("Apply4")
   


class Job4(Base):

    __tablename__ = "jobs4"

    job_id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    skill = Column(String, index=True)
    description = Column(String, index=True)
   
class Apply4(Base):

    __tablename__ = "apply4"

    apply_job_id = Column(Integer, primary_key=True, index=True)
    email = Column(String, ForeignKey("users4.email"))
    job_id = Column(Integer, ForeignKey("jobs4.job_id"))

    user_de = relationship('User4', foreign_keys=[email])
    job_de = relationship("Job4")
