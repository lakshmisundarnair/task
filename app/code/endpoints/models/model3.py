from sqlalchemy import Boolean, Column, ForeignKey, Integer, String,DateTime
from sqlalchemy.orm import relationship
from code.database import Base

class User3(Base):

    __tablename__ = "users3"
    id = Column(Integer, primary_key=True, index=True)
    name=Column(String)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    type = Column(String)
    jobs=relationship("Job3",back_populates="owner")
    application=relationship("Application3",back_populates="applicant")


class Job3(Base):

    __tablename__ = "jobs3"


    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    company_name = Column(String, index=True)
    salary = Column(String, index=True)
    job_type = Column(String, index=True)
    qualification = Column(String, index=True)
    author_id = Column(Integer, ForeignKey("users3.id"))
    created_at=Column(DateTime)
    application=relationship("Application3",back_populates="job")
    owner = relationship("User3",back_populates="jobs")

class Application3(Base):

    __tablename__ = "applications3"

    id = Column(Integer, primary_key=True, index=True)
    job_id = Column(Integer,ForeignKey("jobs3.id"))
    user_id = Column(Integer,ForeignKey("users3.id"))
    introduction = Column(String, index=True)
    qualification = Column(String, index=True)
    skills = Column(String)
    experiance = Column(String)
    phone = Column(Integer)
    submitted_on=Column(DateTime)
    applicant= relationship("User3",back_populates="application")
    job=relationship("Job3",back_populates="application")
