from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import models 
from code.endpoints.crud import crud 
from code import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user3=APIRouter()
from code.database import get_db


@user3.post("/ceate/job/{mail}")#cretae
async def create_job(mail: str,job:schemas.Jobresponse,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=mail)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_chk=db.query(models.Job).filter( models.Job.title==job.title,models.Job.author_id==db_user.id,models.Job.company_name==job.company_name).all()
        if(db_chk !=[]):
            raise HTTPException(status_code=404, detail="This Job Already Exist")
        else:
            db_create=crud.create_job(db,job,id=db_user.id)
        return db_create



@user3.post("/aply/{email}")#apply job
async def create_application(email: str,app:schemas.Applicationres,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=email)
    if (db_user is None):
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.chk_job(db,id=app.job_id)
        if (db_job==[]):
              raise HTTPException(status_code=404, detail="Job not found in that id")
        db_chk=db.query(models.Application).filter( models.Application.job_id==app.job_id,models.Application.user_id==db_user.id).first()
        if(db_chk is None):
            db_app=crud.create_application(db,app,id=db_user.id) 
        else:
            raise HTTPException(status_code=404, detail="Already applied")
    return db_app

@user3.post("/ceate_user")
def create_user(input:schemas.UserCreate,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=input.email)
    if (db_user is not None):
        raise HTTPException(status_code=404, detail="User already found")
    else:
        db_user=crud.create_user(db,input)
    return db_user

