from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model3 as models
from code.endpoints.crud import crud2 as crud 
from code.endpoints.schemas import schema1 as schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user4=APIRouter()
from code.database import get_db
 
@user4.get("/gt/jobs2/{user_email}")#jobcheckuser
async def get_jobs(user_email: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        arr=[]
        query=db.query(models.Job3)
        subquery=db.query(models.Application3.job_id)
        in_expression=models.Job3.id.notin_(subquery.filter(models.Application3.user_id==db_user.id))
        db_job=query.filter(in_expression).all()
        for r in db_job:
            arr.append(r) 
        if(arr==[]):
            raise HTTPException(status_code=404, detail="New Jobs not yet created")

    return arr

@user4.delete("/aplication2/delete/{delid}")#delete application
async def delete_application(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_app(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Application not found in that id")
    else:
        delete=db.query(models.Application3).filter(models.Application3.id==delid).delete()
        db.commit()
    return db.query(models.Application3).all()

@user4.delete("/jb/delete2/{delid}")#delete job
async def delete_Job(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_job(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        delete=db.query(models.Job3).filter(models.Job3.id==delid).delete()
        delete_app=db.query(models.Application3).filter(models.Application3.job_id==delid).delete()
        db.commit()
    return db.query(models.Job).all()

@user4.put("/jb/edit2/{id}")#update job
async def update_Job(id:int,job:schemas.Jobresponse1,db: Session = Depends(get_db)):
    db_job=crud.chk_job(db,id=id)
    if (db_job==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        db_edit=crud.update_job(db,job,id=id)
        
    return db_edit
@user4.post("/ceate/job2/{mail}")#cretae
async def create_job(mail: str,job:schemas.Jobresponse1,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=mail)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_chk=db.query(models.Job3).filter( models.Job3.title==job.title,models.Job3.author_id==db_user.id,models.Job3.company_name==job.company_name).all()
        if(db_chk !=[]):
            raise HTTPException(status_code=404, detail="This Job Already Exist")
        else:
            db_create=crud.create_job(db,job,id=db_user.id)
        return db_create



@user4.post("/aply2/{email}")#apply job
async def create_application(email: str,app:schemas.Applicationres1,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=email)
    if (db_user is None):
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.chk_job(db,id=app.job_id)
        if (db_job==[]):
              raise HTTPException(status_code=404, detail="Job not found in that id")
        db_chk=db.query(models.Application3).filter( models.Application3.job_id==app.job_id,models.Application3.user_id==db_user.id).first()
        if(db_chk is None):
            db_app=crud.create_application(db,app,id=db_user.id) 
        else:
            raise HTTPException(status_code=404, detail="Already applied")
    return db_app

@user4.post("/ceate_user2")
def create_user(input:schemas.UserCreate1,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=input.email)
    if (db_user is not None):
        raise HTTPException(status_code=404, detail="User already found")
    else:
        db_user=crud.create_user(db,input)
    return db_user

