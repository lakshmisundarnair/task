from typing import List, Optional
from datetime import datetime

from pydantic import BaseModel

class User1Base(BaseModel):
    candidate_name: Optional[str] = None
    email: str

    class Config:
        orm_mode = True


class User1Create(User1Base):

    password: str


class User1(User1Base):
    candidate_name: Optional[str] = None
    user_id: int
    is_active: bool

    class Config:
        orm_mode = True

#jobs

class Job1Base(BaseModel):

    title: str
    skill: str
    description: Optional[str] = None

    class Config:
        orm_mode = True


class Job1Create(Job1Base):

    pass


class Job1Update(Job1Base):

    title: str
    skill: str
    description: Optional[str] = None


class Job1(Job1Base):
    job_id: int

    class Config:
        orm_mode = True

# #apply  

class Apply1Base(BaseModel):
    email: str
    job_id: int


class Apply1List(Apply1Base):
    apply_job_id: int
    user_de: Optional[User1Base]
    job_de: Optional[Job1Base]

    class Config:
        orm_mode = True