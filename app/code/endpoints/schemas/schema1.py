from typing import List, Optional
from datetime import datetime

from pydantic import BaseModel

class UserCreate1(BaseModel):
    
    email: str
    name:str
    type:str
    password:str

class UserBase1(BaseModel):
    id:int
    email: str
    name:str
    type:str

class User1(UserBase1):
    email:str


    class Config:
        orm_mode = True

class Application1(BaseModel):
    id:int
    introduction:str
    qualification:str
    phone:int
    skills:str
    experiance:str
    submitted_on: datetime
    applicant:User1

    class Config:
        orm_mode = True

class Job1(BaseModel):
    id:int
    title:str
    company_name:str
    salary:str
    job_type:str
    qualification:str
    author_id:int   
    description:str
    created_at:datetime
    class Config:
        orm_mode = True

class Resjob1(Job1):
    application:List[Application1]
    
    class Config:
        orm_mode = True

class Resapp1(Application1):
    job:Job1
    class Config:
        orm_mode = True

class Baseschema1(BaseModel):
    author_id:int

class Jobresponse1(BaseModel):
        title: str
        company_name: str
        salary: str
        description: str
        job_type: str
        qualification: str

class Applicationres1(BaseModel):
    job_id:int
    intro:str
    qualification:str
    experiance:str
    skills:str
    phone:int


