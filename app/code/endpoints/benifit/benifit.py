from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import models as models 
from code.endpoints.crud import crud as crud
from code import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

router=APIRouter()
from code.database import get_db
@router.get("/")
async def hello():
    return {"hello":"world"}

@router.get("/users28")#user login
async def read_users(db: Session = Depends(get_db)):
    return db.query(models.User).all()


@router.get("/jobs28/{user_email}", response_model=List[schemas.Resjob])#job recruiter
async def read_user(user_email: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.get_job(db,id=db_user.id)
        if db_job is None:
            raise HTTPException(status_code=404, detail="Job not found")
        return db_job

