from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model2 as models 
from code.endpoints.crud import crud1 as crud
from code.endpoints.schemas import schemas
from code.endpoints.crud import crud3 as crud1
from code.endpoints.schemas import schemas as schemas1
from code.endpoints.models import model3 as models1
from code.database import *
from code.database import get_db
models.Base.metadata.create_all(bind=engine)

router=APIRouter()

@router.post("/apply6/user6/", tags=["Apply6"])
def apply_for_jobs(apply_job: schemas.Apply1Base, db: Session = Depends(get_db)):

    return crud.apply_for_jobs(db, apply_job)

@router.put("/jobs6/{job_id}", response_model=schemas.Job1Update, tags=["Jobs6"])
def update_job(job: schemas.Job1Update, job_id=int,  db: Session = Depends(get_db)):

    return crud.job_update(db=db, job_id=job_id, job=job)


@router.delete("/jobs1/{job_id}/", tags=["Jobs1"])
def delete_job(job_id: int, db: Session = Depends(get_db)):
    db_delete = crud.job_delete(db, job_id=job_id)

    return db_delete

#apply


@router.get("/apply6/{job_id}/user6", response_model=List[schemas.Apply1List], tags=["Apply6"])
def apply_list(job_id: int, db: Session = Depends(get_db)):
    db_list = crud.get_list(db, job_id=job_id)

    if db_list is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_list

@router.post("/job40/", response_model=schemas1.Job1, tags=["job40"])
def create_job(
    job: schemas1.Job1Create, db: Session = Depends(get_db)
):
    return crud1.create_user_job(db=db, job=job)


@router.get("/job40/", response_model=List[schemas1.Job1], tags=["job40"])
def read_jobs(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    jobs = crud1.get_jobs(db, skip=skip, limit=limit)
    return jobs


@router.put("/job40/{job_id}", response_model=schemas1.Job1Update, tags=["job40"])
def update_job(job: schemas1.Job1Update, job_id=int,  db: Session = Depends(get_db)):

    return crud1.job_update(db=db, job_id=job_id, job=job)


@router.delete("/job40/{job_id}/", tags=["job40"])
def delete_job(job_id: int, db: Session = Depends(get_db)):
    db_delete = crud1.job_delete(db, job_id=job_id)

    return db_delete
