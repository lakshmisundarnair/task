from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import models as models 
from code.endpoints.crud import crud as crud
from code import schemas
from code.endpoints.crud import crud3 as crud1
from code.endpoints.schemas import schemas as schemas1
from code.endpoints.models import model3 as models1
from code.database import *
from code.database import get_db
models.Base.metadata.create_all(bind=engine)

router=APIRouter()
# Dependency

@router.post("/job40/", response_model=schemas1.Job1, tags=["job40"])
def create_job(
    job: schemas1.Job1Create, db: Session = Depends(get_db)
):
    return crud1.create_user_job(db=db, job=job)


@router.get("/job40/", response_model=List[schemas1.Job1], tags=["job40"])
def read_jobs(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    jobs = crud1.get_jobs(db, skip=skip, limit=limit)
    return jobs


@router.put("/job40/{job_id}", response_model=schemas1.Job1Update, tags=["job40"])
def update_job(job: schemas1.Job1Update, job_id=int,  db: Session = Depends(get_db)):

    return crud1.job_update(db=db, job_id=job_id, job=job)


@router.delete("/job40/{job_id}/", tags=["job40"])
def delete_job(job_id: int, db: Session = Depends(get_db)):
    db_delete = crud1.job_delete(db, job_id=job_id)

    return db_delete

@router.delete("/application4/delete/{delid}")#delete application
async def delete_application(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_app(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Application not found in that id")
    else:
        delete=db.query(models.Application).filter(models.Application.id==delid).delete()
        db.commit()
    return db.query(models.Application).all()

@router.delete("/job4/delete/{delid}")#delete job
async def delete_Job(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_job(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        delete=db.query(models.Job).filter(models.Job.id==delid).delete()
        delete_app=db.query(models.Application).filter(models.Application.job_id==delid).delete()
        db.commit()
    return db.query(models.Job).all()

@router.put("/job4/edit/{id}")#update job
async def update_Job(id:int,job:schemas.Jobresponse,db: Session = Depends(get_db)):
    db_job=crud.chk_job(db,id=id)
    if (db_job==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        db_edit=crud.update_job(db,job,id=id)
        
    return db_edit
@router.post("/create/job4/{mail}")#cretae
async def create_job(mail: str,job:schemas.Jobresponse,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=mail)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_chk=db.query(models.Job).filter( models.Job.title==job.title,models.Job.author_id==db_user.id,models.Job.company_name==job.company_name).all()
        if(db_chk !=[]):
            raise HTTPException(status_code=404, detail="This Job Already Exist")
        else:
            db_create=crud.create_job(db,job,id=db_user.id)
        return db_create



@router.post("/apply4/{email}")#apply job
async def create_application(email: str,app:schemas.Applicationres,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=email)
    if (db_user is None):
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.chk_job(db,id=app.job_id)
        if (db_job==[]):
              raise HTTPException(status_code=404, detail="Job not found in that id")
        db_chk=db.query(models.Application).filter( models.Application.job_id==app.job_id,models.Application.user_id==db_user.id).first()
        if(db_chk is None):
            db_app=crud.create_application(db,app,id=db_user.id) 
        else:
            raise HTTPException(status_code=404, detail="Already applied")
    return db_app

@router.post("/create_user4")
def create_user(input:schemas.UserCreate,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=input.email)
    if (db_user is not None):
        raise HTTPException(status_code=404, detail="User already found")
    else:
        db_user=crud.create_user(db,input)
    return db_user
@router.get("/")
async def hello():
    return {"hello":"world"}

@router.get("/users4")#user login
async def read_users(db: Session = Depends(get_db)):
    return db.query(models.User).all()


@router.get("/jobs4/{user_email}", response_model=List[schemas.Resjob])#job recruiter
async def read_user(user_email: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.get_job(db,id=db_user.id)
        if db_job is None:
            raise HTTPException(status_code=404, detail="Job not found")
        return db_job
