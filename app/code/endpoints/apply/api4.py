from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model3 as models
from code.endpoints.crud import crud2 as crud 
from code.endpoints.schemas import schema1 as schemas
from code.database import get_db
from code.database import *

models.Base.metadata.create_all(bind=engine)

router=APIRouter()

@router.delete("/aplication2/delete/{delid}")#delete application
async def delete_application(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_app(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Application not found in that id")
    else:
        delete=db.query(models.Application3).filter(models.Application3.id==delid).delete()
        db.commit()
    return db.query(models.Application3).all()

@router.delete("/jb/delete2/{delid}")#delete job
async def delete_Job(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_job(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        delete=db.query(models.Job3).filter(models.Job3.id==delid).delete()
        delete_app=db.query(models.Application3).filter(models.Application3.job_id==delid).delete()
        db.commit()
    return db.query(models.Job).all()

@router.put("/jb/edit2/{id}")#update job
async def update_Job(id:int,job:schemas.Jobresponse1,db: Session = Depends(get_db)):
    db_job=crud.chk_job(db,id=id)
    if (db_job==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        db_edit=crud.update_job(db,job,id=id)
        
    return db_edit
@router.post("/ceate/job2/{mail}")#cretae
async def create_job(mail: str,job:schemas.Jobresponse1,db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=mail)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_chk=db.query(models.Job3).filter( models.Job3.title==job.title,models.Job3.author_id==db_user.id,models.Job3.company_name==job.company_name).all()
        if(db_chk !=[]):
            raise HTTPException(status_code=404, detail="This Job Already Exist")
        else:
            db_create=crud.create_job(db,job,id=db_user.id)
        return db_create



