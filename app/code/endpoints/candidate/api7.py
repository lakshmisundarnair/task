from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model4 as models 
from code.endpoints.crud import crud3 as crud
from code.endpoints.schemas import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user5=APIRouter()
# Dependency
from code.database import get_db

@user5.put("/jobs4/{job_id}", response_model=schemas.Job1Update, tags=["Jobs4"])
def update_job(job: schemas.Job1Update, job_id=int,  db: Session = Depends(get_db)):

    return crud.job_update(db=db, job_id=job_id, job=job)


@user5.delete("/jobs4/{job_id}/", tags=["Jobs4"])
def delete_job(job_id: int, db: Session = Depends(get_db)):
    db_delete = crud.job_delete(db, job_id=job_id)

    return db_delete

#apply

@user5.post("/apply4/users1/", tags=["Apply4"])
def apply_for_jobs(apply_job: schemas.Apply1Base, db: Session = Depends(get_db)):

    return crud.apply_for_jobs(db, apply_job)


@user5.get("/apply4/{job_id}/users2", response_model=List[schemas.Apply1List], tags=["Apply4"])
def apply_list(job_id: int, db: Session = Depends(get_db)):
    db_list = crud.get_list(db, job_id=job_id)

    if db_list is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_list
