from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model4 as models 
from code.endpoints.crud import crud3 as crud
from code.endpoints.schemas import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user5=APIRouter()
from code.database import get_db


@user5.delete("/jobs1/{job_id}/", tags=["Jobs2"])
def delete_job(job_id: int, db: Session = Depends(get_db)):
    db_delete = crud.job_delete(db, job_id=job_id)

    return db_delete

#apply

@user5.post("/apply2/users1/", tags=["Apply2"])
def apply_for_jobs(apply_job: schemas.Apply1Base, db: Session = Depends(get_db)):

    return crud.apply_for_jobs(db, apply_job)


@user5.get("/apply2/{job_id}/users2", response_model=List[schemas.Apply1List], tags=["Apply2"])
def apply_list(job_id: int, db: Session = Depends(get_db)):
    db_list = crud.get_list(db, job_id=job_id)

    if db_list is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_list
