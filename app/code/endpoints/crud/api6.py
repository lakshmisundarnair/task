from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model4 as models 
from code.endpoints.crud import crud3 as crud
from code.endpoints.schemas import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user6=APIRouter()
from code.database import get_db

@user6.post("/apply3/users1/", tags=["Apply3"])
def apply_for_jobs(apply_job: schemas.Apply1Base, db: Session = Depends(get_db)):

    return crud.apply_for_jobs(db, apply_job)


@user6.get("/apply2/{job_id}/users3", response_model=List[schemas.Apply1List], tags=["Apply3"])
def apply_list(job_id: int, db: Session = Depends(get_db)):
    db_list = crud.get_list(db, job_id=job_id)

    if db_list is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_list
