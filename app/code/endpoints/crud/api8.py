from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model3 as models
from code.endpoints.crud import crud2 as crud 
from code.endpoints.schemas import schema1 as schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user4=APIRouter()
# Dependency
from code.database import get_db
@user4.get("/hello3")
async def hello():
    return {"hello":"world"}

@user4.get("/usrs3")#user login
async def read_users(db: Session = Depends(get_db)):
    return db.query(models.User3).all()


@user4.get("/jbs3/{user_email}", response_model=List[schemas.Resjob1])#job recruiter
async def read_user(user_email: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.get_job(db,id=db_user.id)
        if db_job is None:
            raise HTTPException(status_code=404, detail="Job not found")
        return db_job


@user4.get("/aplications3/{emailid}", response_model=List[schemas.Resapp1])#user
async def read_application(emailid: str, db: Session = Depends(get_db)):
     db_user = crud.get_user(db, user_email=emailid)
     if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
     else:
        db_app=crud.get_app(db,id=db_user.id)
        if (db_app==[]):
            raise HTTPException(status_code=400, detail="Application not found")
        return db_app

@user4.get("/gt/jobs3/{user_email}")#jobcheckuser
async def get_jobs(user_email: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        arr=[]
        query=db.query(models.Job3)
        subquery=db.query(models.Application3.job_id)
        in_expression=models.Job3.id.notin_(subquery.filter(models.Application3.user_id==db_user.id))
        db_job=query.filter(in_expression).all()
        for r in db_job:
            arr.append(r) 
        if(arr==[]):
            raise HTTPException(status_code=404, detail="New Jobs not yet created")

    return arr

@user4.delete("/aplication3/delete/{delid}")#delete application
async def delete_application(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_app(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Application not found in that id")
    else:
        delete=db.query(models.Application3).filter(models.Application3.id==delid).delete()
        db.commit()
    return db.query(models.Application3).all()
