from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model4 as models 
from code.endpoints.crud import crud3 as crud
from code.endpoints.schemas import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user5=APIRouter()
@user5.post("/users2/", response_model=schemas.User1, tags=["Users2"])
def create_user(user: schemas.User1Create, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)


@user5.get("/users2/", response_model=List[schemas.User1], tags=["Users2"])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = crud.get_users(db, skip=skip, limit=limit)
    return users


@user5.get("/users2/{user_id}", response_model=schemas.User1, tags=["Users2"])
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

#jobs

@user5.post("/jobs2/", response_model=schemas.Job1, tags=["Jobs2"])
def create_job(
    job: schemas.Job1Create, db: Session = Depends(get_db)
):
    return crud.create_user_job(db=db, job=job)


@user5.get("/jobs2/", response_model=List[schemas.Job1], tags=["Jobs2"])
def read_jobs(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    jobs = crud.get_jobs(db, skip=skip, limit=limit)
    return jobs


@user5.put("/jobs2/{job_id}", response_model=schemas.Job1Update, tags=["Jobs2"])
def update_job(job: schemas.Job1Update, job_id=int,  db: Session = Depends(get_db)):

    return crud.job_update(db=db, job_id=job_id, job=job)

