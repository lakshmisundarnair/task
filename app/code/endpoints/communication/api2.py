from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model4 as models1 
from code.endpoints.crud import crud3 as crud
from code.endpoints.schemas import schemas
from code.endpoints.models import model3 as models
from code.endpoints.crud import crud2 as crud2 
from code.endpoints.schemas import schema1 as schemas1
from code.database import *

models.Base.metadata.create_all(bind=engine)

models1.Base.metadata.create_all(bind=engine)
router=APIRouter()
# Dependency
from code.database import get_db
@router.post("/job40/", response_model=schemas.Job1, tags=["job40"])
def create_job(
    job: schemas.Job1Create, db: Session = Depends(get_db)
):
    return crud.create_user_job(db=db, job=job)


@router.get("/job40/", response_model=List[schemas.Job1], tags=["job40"])
def read_jobs(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    jobs = crud.get_jobs(db, skip=skip, limit=limit)
    return jobs


@router.put("/job40/{job_id}", response_model=schemas.Job1Update, tags=["job40"])
def update_job(job: schemas.Job1Update, job_id=int,  db: Session = Depends(get_db)):

    return crud.job_update(db=db, job_id=job_id, job=job)


@router.delete("/job40/{job_id}/", tags=["job40"])
def delete_job(job_id: int, db: Session = Depends(get_db)):
    db_delete = crud.job_delete(db, job_id=job_id)

    return db_delete


@router.get("/aplications2/{emailid}", response_model=List[schemas1.Resapp1])#user
async def read_application(emailid: str, db: Session = Depends(get_db)):
     db_user = crud2.get_user(db, user_email=emailid)
     if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
     else:
        db_app=crud.get_app(db,id=db_user.id)
        if (db_app==[]):
            raise HTTPException(status_code=400, detail="Application not found")
        return db_app

@router.get("/gt/jobs2/{user_email}")#jobcheckuser
async def get_jobs(user_email: str, db: Session = Depends(get_db)):
    db_user = crud2.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        arr=[]
        query=db.query(models.Job3)
        subquery=db.query(models.Application3.job_id)
        in_expression=models.Job3.id.notin_(subquery.filter(models.Application3.user_id==db_user.id))
        db_job=query.filter(in_expression).all()
        for r in db_job:
            arr.append(r) 
        if(arr==[]):
            raise HTTPException(status_code=404, detail="New Jobs not yet created")

    return arr

@router.post("/ceate_user2")
def create_user(input:schemas1.UserCreate1,db: Session = Depends(get_db)):
    db_user = crud2.get_user(db, user_email=input.email)
    if (db_user is not None):
        raise HTTPException(status_code=404, detail="User already found")
    else:
        db_user=crud2.create_user(db,input)
    return db_user
