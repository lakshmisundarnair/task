from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model4 as models 
from code.endpoints.crud import crud3 as crud
from code.endpoints.schemas import schemas
from code.database import get_db
from code.database import *

models.Base.metadata.create_all(bind=engine)

router=APIRouter()

@router.post("/job26/", response_model=schemas.Job1, tags=["job26"])
def create_job(
    job: schemas.Job1Create, db: Session = Depends(get_db)
):
    return crud.create_user_job(db=db, job=job)


@router.get("/job26/", response_model=List[schemas.Job1], tags=["job26"])
def read_jobs(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    jobs = crud.get_jobs(db, skip=skip, limit=limit)
    return jobs


@router.put("/job26/{job_id}", response_model=schemas.Job1Update, tags=["job26"])
def update_job(job: schemas.Job1Update, job_id=int,  db: Session = Depends(get_db)):

    return crud.job_update(db=db, job_id=job_id, job=job)


@router.delete("/job26/{job_id}/", tags=["job26"])
def delete_job(job_id: int, db: Session = Depends(get_db)):
    db_delete = crud.job_delete(db, job_id=job_id)

    return db_delete