from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import models 
from code.endpoints.crud import crud 
from code import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user3=APIRouter()
# Dependency
from code.database import get_db
@user3.get("/hello")
async def hello():
    return {"hello":"world"}

@user3.get("/usrs")#user login
async def read_users(db: Session = Depends(get_db)):
    return db.query(models.User).all()


@user3.get("/jbs/{user_email}", response_model=List[schemas.Resjob])#job recruiter
async def read_user(user_email: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.get_job(db,id=db_user.id)
        if db_job is None:
            raise HTTPException(status_code=404, detail="Job not found")
        return db_job


@user3.get("/aplications/{emailid}", response_model=List[schemas.Resapp])#user
async def read_application(emailid: str, db: Session = Depends(get_db)):
     db_user = crud.get_user(db, user_email=emailid)
     if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
     else:
        db_app=crud.get_app(db,id=db_user.id)
        if (db_app==[]):
            raise HTTPException(status_code=400, detail="Application not found")
        return db_app

@user3.get("/gt/jobs/{user_email}")#jobcheckuser
async def get_jobs(user_email: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        arr=[]
        query=db.query(models.Job)
        subquery=db.query(models.Application.job_id)
        in_expression=models.Job.id.notin_(subquery.filter(models.Application.user_id==db_user.id))
        db_job=query.filter(in_expression).all()
        for r in db_job:
            arr.append(r) 
        if(arr==[]):
            raise HTTPException(status_code=404, detail="New Jobs not yet created")

    return arr

@user3.delete("/aplication/delete/{delid}")#delete application
async def delete_application(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_app(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Application not found in that id")
    else:
        delete=db.query(models.Application).filter(models.Application.id==delid).delete()
        db.commit()
    return db.query(models.Application).all()

@user3.delete("/jb/delete/{delid}")#delete job
async def delete_Job(delid:int,db: Session = Depends(get_db)):
    db_app=crud.chk_job(db,id=delid)
    if (db_app==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        delete=db.query(models.Job).filter(models.Job.id==delid).delete()
        delete_app=db.query(models.Application).filter(models.Application.job_id==delid).delete()
        db.commit()
    return db.query(models.Job).all()

@user3.put("/jb/edit/{id}")#update job
async def update_Job(id:int,job:schemas.Jobresponse,db: Session = Depends(get_db)):
    db_job=crud.chk_job(db,id=id)
    if (db_job==[]):
        raise HTTPException(status_code=404, detail="Job not found in that id")
    else:
        db_edit=crud.update_job(db,job,id=id)
        
    return db_edit