from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import model2 as models 
from code.endpoints.crud import crud1 as crud
from code.endpoints.schemas import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

router=APIRouter()
# Dependency
from code.database import get_db

#jobs

@router.post("/jobs13/", response_model=schemas.Job1, tags=["Jobs13"])
def create_job(
    job: schemas.Job1Create, db: Session = Depends(get_db)
):
    return crud.create_user_job(db=db, job=job)


@router.get("/jobs13/", response_model=List[schemas.Job1], tags=["Jobs13"])
def read_jobs(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    jobs = crud.get_jobs(db, skip=skip, limit=limit)
    return jobs


@router.put("/jobs13/{job_id}", response_model=schemas.Job1Update, tags=["Jobs13"])
def update_job(job: schemas.Job1Update, job_id=int,  db: Session = Depends(get_db)):

    return crud.job_update(db=db, job_id=job_id, job=job)


@router.delete("/jobs13/{job_id}/", tags=["Jobs13"])
def delete_job(job_id: int, db: Session = Depends(get_db)):
    db_delete = crud.job_delete(db, job_id=job_id)

    return db_delete

#apply

@router.post("/apply13/user6/", tags=["Apply13"])
def apply_for_jobs(apply_job: schemas.Apply1Base, db: Session = Depends(get_db)):

    return crud.apply_for_jobs(db, apply_job)


@router.get("/apply13/{job_id}/user6", response_model=List[schemas.Apply1List], tags=["Apply13"])
def apply_list(job_id: int, db: Session = Depends(get_db)):
    db_list = crud.get_list(db, job_id=job_id)

    if db_list is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_list
