from typing import List
from fastapi import APIRouter
from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from code.endpoints.models import models as models 
from code.endpoints.crud import crud as crud
from code import schemas

from code.database import *

models.Base.metadata.create_all(bind=engine)

user2=APIRouter()
# Dependency
from code.database import get_db
@user2.get("/")
async def hello():
    return {"hello":"world"}

@user2.get("/users")#user login
async def read_users(db: Session = Depends(get_db)):
    return db.query(models.User).all()


@user2.get("/jobs/{user_email}", response_model=List[schemas.Resjob])#job recruiter
async def read_user(user_email: str, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_email=user_email)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        db_job=crud.get_job(db,id=db_user.id)
        if db_job is None:
            raise HTTPException(status_code=404, detail="Job not found")
        return db_job


@user2.get("/applications/{emailid}", response_model=List[schemas.Resapp])#user
async def read_application(emailid: str, db: Session = Depends(get_db)):
     db_user = crud.get_user(db, user_email=emailid)
     if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
     else:
        db_app=crud.get_app(db,id=db_user.id)
        if (db_app==[]):
            raise HTTPException(status_code=400, detail="Application not found")
        return db_app
