from fastapi import APIRouter
from code.endpoints.user import api1,api5 as user5
from code.endpoints.user import api2
from code.endpoints.user import api3
from code.endpoints.user import api4

from code.endpoints.candidate import api5
from code.endpoints.candidate import api6
from code.endpoints.candidate import api7
from code.endpoints.candidate import api8
from code.endpoints.data import api1 as api9
from code.endpoints.data import api2  as api10
from code.endpoints.data import api3 as api11
from code.endpoints.data import api4 as api12
from code.endpoints.alert import alert,alert1,alert2,alert3,alert4,alert5,alert6,alert7
from code.endpoints.card import api5 as api13
from code.endpoints.card import api6 as api14
from code.endpoints.card import api7 as api15
from code.endpoints.card import api8 as api16
from code.endpoints.data import api
from code.endpoints.api import api1 as api17,api5 as apii5,api6 as apii6,api7 as apii7 ,api8 as apii8
from code.endpoints.api import api2  as api18
from code.endpoints.api import api3 as api19
from code.endpoints.api import api4 as api20
from code.endpoints.crud import api5 as api21
from code.endpoints.crud import api6 as api22
from code.endpoints.crud import api7 as api23
from code.endpoints.crud import api8 as api24
from code.endpoints.action import api1 as api25,api2 as api26,api3 as api27,api4 as api28,action1,action
from code.endpoints.view import api5 as api29,api6 as api30,api7 as api31,api8 as api32
from code.endpoints.connection import api1 as api33,api2 as api34,api3 as api35,api4 as api36,api5 as api37
from code.endpoints.job import api as api41,api1 as api42,api2 as api43,api3 as api44,api4 as api45
from code.endpoints.recruiter import api5 as api46,api6 as api47,api7 as api49,api8 as api48
from code.endpoints.models import api as api50,api1 as api52,api2 as api53,api3 as api54
from code.endpoints.apply import api4 as api55,api5 as api56,api6 as api57,api7 as api58,api8 as api51
from code.endpoints.register import register,register1,register4,register2
from code.endpoints.organization import api5 as api40,api6 as api38, api7 as api39,api8 as api59
from code.endpoints.benifit import benifit,benifit1,benifit2,benifit3
from code.endpoints.test import test,test1,test3,test4
from code.endpoints.mail import mail,mail1,mail2,mail3,mail4,mail5
from code.endpoints.login import login,login1,login2,login3,login4,login5
from code.endpoints.communication import api as comm,api1 as comm2,api2 as comm3 ,api3 as comm4
from code.endpoints.location import location,location2,location1
user=APIRouter()
user.include_router(register4.router)
user.include_router(register2.router)
user.include_router(test4.router)
user.include_router(user5.user3)
user.include_router(api38.router)
user.include_router(api39.router)
user.include_router(api59.router)
user.include_router(action.router)
user.include_router(action1.router)
user.include_router(api1.user2,tags=["sample1"])
user.include_router(api2.user3,tags=["sample2"])
user.include_router(api4.user4,tags=["sample3"])
user.include_router(api3.user3)
user.include_router(api5.user5)
user.include_router(apii5.router)
user.include_router(apii6.router)
user.include_router(apii7.router)
user.include_router(apii8.router)
user.include_router(api7.user5)
user.include_router(api6.user6)
user.include_router(api.user2,tags=["sample4"])
user.include_router(api8.user4,tags=["sample5"])
user.include_router(api9.router,tags=["sample6"])
user.include_router(api10.router,tags=["sample7"])
user.include_router(api11.router,tags=["sample8"])
user.include_router(api12.router)
user.include_router(api13.router)
user.include_router(api13.router)
user.include_router(api15.router)
user.include_router(alert.router)
user.include_router(alert1.router)
user.include_router(alert2.router)
user.include_router(alert3.router)
user.include_router(alert4.router)
user.include_router(alert6.router)
user.include_router(alert7.router)
user.include_router(alert5.user4)
user.include_router(api16.router,tags=["sample9"])
user.include_router(api17.router,tags=["sample10"])
user.include_router(api18.router,tags=["sample11"])
user.include_router(api19.router,tags=["sample12"])
user.include_router(api22.user6)
user.include_router(api21.user5)
user.include_router(api20.router)
user.include_router(api23.user5)
user.include_router(api24.user4,tags=["sample13"])
user.include_router(register.router)
user.include_router(register1.router)
user.include_router(api25.router,tags=["sample14"])
user.include_router(api26.router,tags=["sample15"])
user.include_router(api27.router,tags=["sample16"])
user.include_router(api28.router)
user.include_router(api29.router)
user.include_router(api30.router)
user.include_router(api31.router)
user.include_router(api32.router,tags=["sample17"])
user.include_router(api36.user4,tags=["sample19"])
user.include_router(api37.router,tags=["sample20"])
user.include_router(api35.user3)
user.include_router(api34.user3)
user.include_router(api33.user2,tags=["sample21"])
user.include_router(api41.user2,tags=["sample22"])
user.include_router(api42.router,tags=["sample23"])
user.include_router(api43.router,tags=["sample24"])
user.include_router(api48.router,tags=["sample29"])
user.include_router(api49.router,tags=["sample28"])
user.include_router(api47.router,tags=["sample27"])
user.include_router(api45.router,tags=["sample26"])
user.include_router(api44.router,tags=["sample25"])
user.include_router(api46.router,tags=["sample"])
user.include_router(api50.user2,tags=["sample30"])
user.include_router(api52.router,tags=["sample33"])
user.include_router(api53.router,tags=["sample34"])
user.include_router(api58.router,tags=["sample31"])
user.include_router(api57.router,tags=["sample35"])
user.include_router(api56.router,tags=["sample36"])
user.include_router(api55.router,tags=["sample37"])
user.include_router(api54.router,tags=["sample38"])
user.include_router(api51.router,tags=["sample39"])
user.include_router(benifit.router)
user.include_router(benifit1.router)
user.include_router(benifit2.router)
user.include_router(benifit3.router)
user.include_router(test.router)
user.include_router(test1.router)
user.include_router(test3.router)
user.include_router(mail.router)
user.include_router(mail1.router)
user.include_router(mail2.router)
user.include_router(mail3.router)
user.include_router(mail5.router)
user.include_router(mail4.router)
user.include_router(location.router)
user.include_router(location1.router)
user.include_router(location2.router)
user.include_router(login.router)
user.include_router(login4.router)
user.include_router(login3.router)
user.include_router(login1.router)
user.include_router(login2.router)
user.include_router(login5.router)
user.include_router(comm.router)
user.include_router(comm2.router)
user.include_router(comm3.router)
user.include_router(comm4.router)