#! /bin/sh
set -e



PRE_START_PATH=${PRE_START_PATH:-/app/loader.sh}
echo "Checking for script in $PRE_START_PATH"
if [ -f $PRE_START_PATH ] ; then
    echo "Running script $PRE_START_PATH"
    . "$PRE_START_PATH"
else
    echo "There is no script $PRE_START_PATH"
fi
cd /app
#gunicorn index:app -w 1 --bind 0.0.0.0:80 -k uvicorn.workers.UvicornWorker --reload --error-logfile "-"  --enable-stdio-inheritance

uvicorn index:app --host 0.0.0.0 --port 80 --reload